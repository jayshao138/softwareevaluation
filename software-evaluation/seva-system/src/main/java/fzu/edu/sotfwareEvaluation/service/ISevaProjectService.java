package fzu.edu.sotfwareEvaluation.service;

import java.util.List;
import fzu.edu.sotfwareEvaluation.domain.SevaProject;

/**
 * 项目管理Service接口
 *
 * @author jayerListen
 * @date 2024-04-01
 */
public interface ISevaProjectService
{
    /**
     * 查询项目管理
     *
     * @param id 项目管理主键
     * @return 项目管理
     */
    public SevaProject selectSevaProjectById(Long id);

    /**
     * 查询项目管理列表
     *
     * @param sevaProject 项目管理
     * @return 项目管理集合
     */
    public List<SevaProject> selectSevaProjectList(SevaProject sevaProject);

    /**
     * 新增项目管理
     *
     * @param sevaProject 项目管理
     * @return 结果
     */
    public int insertSevaProject(SevaProject sevaProject);

    /**
     * 修改项目管理
     *
     * @param sevaProject 项目管理
     * @return 结果
     */
    public int updateSevaProject(SevaProject sevaProject);

    /**
     * 批量删除项目管理
     *
     * @param ids 需要删除的项目管理主键集合
     * @return 结果
     */
    public int deleteSevaProjectByIds(Long[] ids);

    /**
     * 删除项目管理信息
     *
     * @param id 项目管理主键
     * @return 结果
     */
    public int deleteSevaProjectById(Long id);
}
