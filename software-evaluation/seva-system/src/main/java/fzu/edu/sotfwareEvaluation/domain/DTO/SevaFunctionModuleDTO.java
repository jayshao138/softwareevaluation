package fzu.edu.sotfwareEvaluation.domain.DTO;

import fzu.edu.common.annotation.Excel;

import java.io.Serializable;

/**
 * @author JayerListen
 * @create 2024-08-13 2024/8/13
 */
public class SevaFunctionModuleDTO implements Serializable {

    /** 模块名称 */
    @Excel(name = "模块名称")
    private String funtionName;

    /** 模块内容 */
    @Excel(name = "模块内容")
    private String functionDetail;

    /** 模块分类 */
    @Excel(name = "模块分类")
    private String functionTypeName;

    public String getFuntionName() {
        return funtionName;
    }

    public void setFuntionName(String funtionName) {
        this.funtionName = funtionName;
    }

    public String getFunctionDetail() {
        return functionDetail;
    }

    public void setFunctionDetail(String functionDetail) {
        this.functionDetail = functionDetail;
    }

    public String getFunctionTypeName() {
        return functionTypeName;
    }

    public void setFunctionTypeName(String functionTypeName) {
        this.functionTypeName = functionTypeName;
    }

    @Override
    public String toString() {
        return "SevaFunctionModuleDTO{" +
                "funtionName='" + funtionName + '\'' +
                ", functionDetail='" + functionDetail + '\'' +
                ", functionTypeName='" + functionTypeName + '\'' +
                '}';
    }
}
