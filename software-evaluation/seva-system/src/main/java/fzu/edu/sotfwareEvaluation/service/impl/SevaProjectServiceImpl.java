package fzu.edu.sotfwareEvaluation.service.impl;

import java.util.List;
import fzu.edu.common.utils.DateUtils;
import fzu.edu.sotfwareEvaluation.domain.SevaProject;
import fzu.edu.sotfwareEvaluation.mapper.SevaProjectMapper;
import fzu.edu.sotfwareEvaluation.service.ISevaProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 项目管理Service业务层处理
 *
 * @author jayerListen
 * @date 2024-04-01
 */
@Service
public class SevaProjectServiceImpl implements ISevaProjectService
{
    @Autowired
    private SevaProjectMapper sevaProjectMapper;

    /**
     * 查询项目管理
     *
     * @param id 项目管理主键
     * @return 项目管理
     */
    @Override
    public SevaProject selectSevaProjectById(Long id)
    {
        return sevaProjectMapper.selectSevaProjectById(id);
    }

    /**
     * 查询项目管理列表
     *
     * @param sevaProject 项目管理
     * @return 项目管理
     */
    @Override
    public List<SevaProject> selectSevaProjectList(SevaProject sevaProject)
    {
        return sevaProjectMapper.selectSevaProjectList(sevaProject);
    }

    /**
     * 新增项目管理
     *
     * @param sevaProject 项目管理
     * @return 结果
     */
    @Override
    public int insertSevaProject(SevaProject sevaProject)
    {
        sevaProject.setCreateTime(DateUtils.getNowDate());
        return sevaProjectMapper.insertSevaProject(sevaProject);
    }

    /**
     * 修改项目管理
     *
     * @param sevaProject 项目管理
     * @return 结果
     */
    @Override
    public int updateSevaProject(SevaProject sevaProject)
    {
        sevaProject.setUpdateTime(DateUtils.getNowDate());
        return sevaProjectMapper.updateSevaProject(sevaProject);
    }

    /**
     * 批量删除项目管理
     *
     * @param ids 需要删除的项目管理主键
     * @return 结果
     */
    @Override
    public int deleteSevaProjectByIds(Long[] ids)
    {
        return sevaProjectMapper.deleteSevaProjectByIds(ids);
    }

    /**
     * 删除项目管理信息
     *
     * @param id 项目管理主键
     * @return 结果
     */
    @Override
    public int deleteSevaProjectById(Long id)
    {
        return sevaProjectMapper.deleteSevaProjectById(id);
    }
}
