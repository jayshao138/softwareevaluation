package fzu.edu.sotfwareEvaluation.mapper;

import java.util.List;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionType;

/**
 * 基线速查表Mapper接口
 *
 * @author JayerListen
 * @date 2024-08-12
 */
public interface SevaFunctionTypeMapper
{
    /**
     * 查询基线速查表
     *
     * @param id 基线速查表主键
     * @return 基线速查表
     */
    public SevaFunctionType selectSevaFunctionTypeById(Long id);

    /**
     * 查询基线速查表
     *
     * @param id 基线速查表主键
     * @return 基线速查表
     */
    public SevaFunctionType selectSevaFunctionTypeByCode(String code);

    /**
     * 查询基线速查表列表
     *
     * @param sevaFunctionType 基线速查表
     * @return 基线速查表集合
     */
    public List<SevaFunctionType> selectSevaFunctionTypeList(SevaFunctionType sevaFunctionType);

    /**
     * 新增基线速查表
     *
     * @param sevaFunctionType 基线速查表
     * @return 结果
     */
    public int insertSevaFunctionType(SevaFunctionType sevaFunctionType);

    /**
     * 修改基线速查表
     *
     * @param sevaFunctionType 基线速查表
     * @return 结果
     */
    public int updateSevaFunctionType(SevaFunctionType sevaFunctionType);

    /**
     * 删除基线速查表
     *
     * @param id 基线速查表主键
     * @return 结果
     */
    public int deleteSevaFunctionTypeById(Long id);

    /**
     * 批量删除基线速查表
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSevaFunctionTypeByIds(Long[] ids);
}
