package fzu.edu.sotfwareEvaluation.service;

import java.util.List;
import fzu.edu.sotfwareEvaluation.domain.SevaParameter;

/**
 * 行业基准参数Service接口
 *
 * @author jayerlisten
 * @date 2024-04-01
 */
public interface ISevaParameterService
{
    /**
     * 查询行业基准参数
     *
     * @param id 行业基准参数主键
     * @return 行业基准参数
     */
    public SevaParameter selectSevaParameterById(Long id);

    /**
     * 查询行业基准参数列表
     *
     * @param sevaParameter 行业基准参数
     * @return 行业基准参数集合
     */
    public List<SevaParameter> selectSevaParameterList(SevaParameter sevaParameter);

    /**
     * 新增行业基准参数
     *
     * @param sevaParameter 行业基准参数
     * @return 结果
     */
    public int insertSevaParameter(SevaParameter sevaParameter);

    /**
     * 修改行业基准参数
     *
     * @param sevaParameter 行业基准参数
     * @return 结果
     */
    public int updateSevaParameter(SevaParameter sevaParameter);

    /**
     * 批量删除行业基准参数
     *
     * @param ids 需要删除的行业基准参数主键集合
     * @return 结果
     */
    public int deleteSevaParameterByIds(Long[] ids);

    /**
     * 删除行业基准参数信息
     *
     * @param id 行业基准参数主键
     * @return 结果
     */
    public int deleteSevaParameterById(Long id);
}
