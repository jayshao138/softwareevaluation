package fzu.edu.sotfwareEvaluation.domain;

import fzu.edu.common.annotation.Excel;
import fzu.edu.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 功能模块对象 seva_function_module
 *
 * @author JayerListen
 * @date 2024-08-10
 */
public class SevaFunctionModule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 模块名称 */
    @Excel(name = "模块名称")
    private String funtionName;

    /** 模块内容 */
    @Excel(name = "模块内容")
    private String functionDetail;

    /** 项目id */
    private Long projectId;

    /** 模块分类 */
    private Long functionTypeId;

    /** 是否处理 */
    private String isHandle;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setFuntionName(String funtionName)
    {
        this.funtionName = funtionName;
    }

    public String getFuntionName()
    {
        return funtionName;
    }
    public void setFunctionDetail(String functionDetail)
    {
        this.functionDetail = functionDetail;
    }

    public String getFunctionDetail()
    {
        return functionDetail;
    }
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public Long getProjectId()
    {
        return projectId;
    }
    public void setFunctionTypeId(Long functionTypeId)
    {
        this.functionTypeId = functionTypeId;
    }

    public Long getFunctionTypeId()
    {
        return functionTypeId;
    }

    public String getIsHandle() {
        return isHandle;
    }

    public void setIsHandle(String isHandle) {
        this.isHandle = isHandle;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("funtionName", getFuntionName())
            .append("functionDetail", getFunctionDetail())
            .append("projectId", getProjectId())
            .append("functionTypeId", getFunctionTypeId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
