package fzu.edu.sotfwareEvaluation.domain;

import fzu.edu.common.annotation.Excel;
import fzu.edu.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 项目管理对象 seva_project
 *
 * @author jayerListen
 * @date 2024-04-01
 */
public class SevaProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 项目负责人 */
    @Excel(name = "项目负责人")
    private String projectLeader;

    /** 负责人电话 */
    @Excel(name = "负责人电话")
    private String projectLeaderPhone;

    /** 项目评估价格 */
    @Excel(name = "项目评估价格")
    private Long projectPrice;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getProjectName()
    {
        return projectName;
    }
    public void setProjectLeader(String projectLeader)
    {
        this.projectLeader = projectLeader;
    }

    public String getProjectLeader()
    {
        return projectLeader;
    }
    public void setProjectLeaderPhone(String projectLeaderPhone)
    {
        this.projectLeaderPhone = projectLeaderPhone;
    }

    public String getProjectLeaderPhone()
    {
        return projectLeaderPhone;
    }
    public void setProjectPrice(Long projectPrice)
    {
        this.projectPrice = projectPrice;
    }

    public Long getProjectPrice()
    {
        return projectPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectName", getProjectName())
            .append("projectLeader", getProjectLeader())
            .append("projectLeaderPhone", getProjectLeaderPhone())
            .append("projectPrice", getProjectPrice())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
