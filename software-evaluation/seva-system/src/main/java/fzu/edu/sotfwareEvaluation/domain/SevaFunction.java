package fzu.edu.sotfwareEvaluation.domain;

import fzu.edu.common.annotation.Excel;
import fzu.edu.common.core.domain.TreeEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 功能模块对象 seva_function
 *
 * @author jayerListen
 * @date 2024-04-02
 */
public class SevaFunction extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 项目id */
    private Long projectId;

    /** 功能模块名称 */
    @Excel(name = "功能模块名称")
    private String funtionName;

    /** 功能描述 */
    private String funtionDetail;

    /** 内部逻辑文件 */
    @Excel(name = "内部逻辑文件")
    private Long ilf;

    /** 外部接口文件 */
    @Excel(name = "外部接口文件")
    private Long eif;

    /** 外部输入 */
    @Excel(name = "外部输入")
    private Long ei;

    /** 外部输出 */
    @Excel(name = "外部输出")
    private Long eo;

    /** 外部查询 */
    @Excel(name = "外部查询")
    private Long eq;

    /** 应用类型调节因子 */
    @Excel(name = "应用类型调节因子")
    private Long typeFactor;

    /** 重复性调节因子 */
    @Excel(name = "重复性调节因子")
    private Long repeatFactor;

    /** 应用要求调节因子(又分布式/性能/可靠性/多重站点适配等因子计算决定) */
    @Excel(name = "应用要求调节因子(又分布式/性能/可靠性/多重站点适配等因子计算决定)")
    private Long requirementFactor;

    /** 分布式要求因子 */
    private Long distributionFactor;

    /** 性能要求因子 */
    private Long performanceFactor;

    /** 可靠性要求因子 */
    private Long reliabilityFactor;

    /** 多重站点适配要求因子 */
    private Long adaptiveFactor;

    /** 功能点数 */
    @Excel(name = "功能点数")
    private Long funtionPoint;

    /** 是否为叶节点（0正常 1停用） */
    private String isLeaf;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public Long getProjectId()
    {
        return projectId;
    }
    public void setFuntionName(String funtionName)
    {
        this.funtionName = funtionName;
    }

    public String getFuntionName()
    {
        return funtionName;
    }
    public void setFuntionDetail(String funtionDetail)
    {
        this.funtionDetail = funtionDetail;
    }

    public String getFuntionDetail()
    {
        return funtionDetail;
    }
    public void setIlf(Long ilf)
    {
        this.ilf = ilf;
    }

    public Long getIlf()
    {
        return ilf;
    }
    public void setEif(Long eif)
    {
        this.eif = eif;
    }

    public Long getEif()
    {
        return eif;
    }
    public void setEi(Long ei)
    {
        this.ei = ei;
    }

    public Long getEi()
    {
        return ei;
    }
    public void setEo(Long eo)
    {
        this.eo = eo;
    }

    public Long getEo()
    {
        return eo;
    }
    public void setEq(Long eq)
    {
        this.eq = eq;
    }

    public Long getEq()
    {
        return eq;
    }
    public void setTypeFactor(Long typeFactor)
    {
        this.typeFactor = typeFactor;
    }

    public Long getTypeFactor()
    {
        return typeFactor;
    }
    public void setRepeatFactor(Long repeatFactor)
    {
        this.repeatFactor = repeatFactor;
    }

    public Long getRepeatFactor()
    {
        return repeatFactor;
    }
    public void setRequirementFactor(Long requirementFactor)
    {
        this.requirementFactor = requirementFactor;
    }

    public Long getRequirementFactor()
    {
        return requirementFactor;
    }
    public void setDistributionFactor(Long distributionFactor)
    {
        this.distributionFactor = distributionFactor;
    }

    public Long getDistributionFactor()
    {
        return distributionFactor;
    }
    public void setPerformanceFactor(Long performanceFactor)
    {
        this.performanceFactor = performanceFactor;
    }

    public Long getPerformanceFactor()
    {
        return performanceFactor;
    }
    public void setReliabilityFactor(Long reliabilityFactor)
    {
        this.reliabilityFactor = reliabilityFactor;
    }

    public Long getReliabilityFactor()
    {
        return reliabilityFactor;
    }
    public void setAdaptiveFactor(Long adaptiveFactor)
    {
        this.adaptiveFactor = adaptiveFactor;
    }

    public Long getAdaptiveFactor()
    {
        return adaptiveFactor;
    }
    public void setFuntionPoint(Long funtionPoint)
    {
        this.funtionPoint = funtionPoint;
    }

    public Long getFuntionPoint()
    {
        return funtionPoint;
    }
    public void setIsLeaf(String isLeaf)
    {
        this.isLeaf = isLeaf;
    }

    public String getIsLeaf()
    {
        return isLeaf;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectId", getProjectId())
            .append("parentId", getParentId())
            .append("ancestors", getAncestors())
            .append("funtionName", getFuntionName())
            .append("funtionDetail", getFuntionDetail())
            .append("ilf", getIlf())
            .append("eif", getEif())
            .append("ei", getEi())
            .append("eo", getEo())
            .append("eq", getEq())
            .append("typeFactor", getTypeFactor())
            .append("repeatFactor", getRepeatFactor())
            .append("requirementFactor", getRequirementFactor())
            .append("distributionFactor", getDistributionFactor())
            .append("performanceFactor", getPerformanceFactor())
            .append("reliabilityFactor", getReliabilityFactor())
            .append("adaptiveFactor", getAdaptiveFactor())
            .append("funtionPoint", getFuntionPoint())
            .append("orderNum", getOrderNum())
            .append("isLeaf", getIsLeaf())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
