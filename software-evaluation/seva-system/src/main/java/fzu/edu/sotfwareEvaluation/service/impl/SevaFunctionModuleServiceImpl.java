package fzu.edu.sotfwareEvaluation.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.github.pagehelper.PageInfo;
import fzu.edu.common.utils.DateUtils;
import fzu.edu.sotfwareEvaluation.domain.DTO.SevaFunctionModuleDTO;
import fzu.edu.sotfwareEvaluation.domain.DTO.SevaFunctionModuleTypeResultDTO;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionType;
import fzu.edu.sotfwareEvaluation.domain.SevaParameter;
import fzu.edu.sotfwareEvaluation.domain.VO.SevaFunctionModuleVO;
import fzu.edu.sotfwareEvaluation.mapper.SevaFunctionTypeMapper;
import fzu.edu.sotfwareEvaluation.mapper.SevaParameterMapper;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionModule;
import fzu.edu.sotfwareEvaluation.mapper.SevaFunctionModuleMapper;
import fzu.edu.sotfwareEvaluation.service.ISevaFunctionModuleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 功能模块Service业务层处理
 *
 * @author JayerListen
 * @date 2024-08-10
 */
@Service
public class SevaFunctionModuleServiceImpl implements ISevaFunctionModuleService
{
    @Autowired
    private SevaFunctionModuleMapper sevaFunctionModuleMapper;

    @Autowired
    private SevaFunctionTypeMapper sevaFunctionTypeMapper;

    @Autowired
    private SevaParameterMapper sevaParameterMapper;

    /**
     * 查询功能模块
     *
     * @param id 功能模块主键
     * @return 功能模块
     */
    @Override
    public SevaFunctionModule selectSevaFunctionModuleById(Long id)
    {
        return sevaFunctionModuleMapper.selectSevaFunctionModuleById(id);
    }

    /**
     * 查询功能模块列表
     *
     * @param sevaFunctionModule 功能模块
     * @return 功能模块
     */
    @Override
    public List<SevaFunctionModuleVO> selectSevaFunctionModuleList(SevaFunctionModule sevaFunctionModule)
    {
        List<SevaFunctionModule> pos =
                sevaFunctionModuleMapper.selectSevaFunctionModuleList(sevaFunctionModule);
        PageInfo pages = new PageInfo<>(pos);
        List<SevaFunctionModuleVO> vos = new ArrayList<>();
        for(SevaFunctionModule po:pos){
            SevaFunctionModuleVO vo = new SevaFunctionModuleVO();
            BeanUtils.copyProperties(po,vo);
            Long typeId = po.getFunctionTypeId();
            SevaFunctionType type = sevaFunctionTypeMapper.selectSevaFunctionTypeById(typeId);
            vo.setFunctionTypeName(type==null?null:type.getFunctionTypeName());
            vos.add(vo);
        }
        pages.setList(vos);
        return vos;
    }

    /**
     * 新增功能模块
     *
     * @param sevaFunctionModule 功能模块
     * @return 结果
     */
    @Override
    public int insertSevaFunctionModule(SevaFunctionModule sevaFunctionModule)
    {
        sevaFunctionModule.setCreateTime(DateUtils.getNowDate());
        if(sevaFunctionModule.getFunctionTypeId()==null ) sevaFunctionModule.setIsHandle("0");
        else sevaFunctionModule.setIsHandle("1");
        return sevaFunctionModuleMapper.insertSevaFunctionModule(sevaFunctionModule);
    }

    @Override
    @Transactional
    public int batchInsertSevaFunctionModule(List<SevaFunctionModuleDTO> list, Long projectId) {
        int i =0;
        for(SevaFunctionModuleDTO dto:list){
            SevaFunctionModule po = new SevaFunctionModule();
            BeanUtils.copyProperties(dto,po);
            po.setProjectId(projectId);
            if(dto.getFunctionTypeName()!=null && !dto.getFunctionTypeName().isEmpty()){
                SevaFunctionType type =
                        sevaFunctionTypeMapper.selectSevaFunctionTypeByCode(dto.getFunctionTypeName());
                po.setFunctionTypeId(type.getId());
            }
            i = insertSevaFunctionModule(po);
        }
        return i;
    }

    /**
     * 修改功能模块
     *
     * @param sevaFunctionModule 功能模块
     * @return 结果
     */
    @Override
    public int updateSevaFunctionModule(SevaFunctionModule sevaFunctionModule)
    {
        sevaFunctionModule.setUpdateTime(DateUtils.getNowDate());
        if(sevaFunctionModule.getFunctionTypeId()!=null) sevaFunctionModule.setIsHandle("1");
        return sevaFunctionModuleMapper.updateSevaFunctionModule(sevaFunctionModule);
    }

    /**
     * 批量删除功能模块
     *
     * @param ids 需要删除的功能模块主键
     * @return 结果
     */
    @Override
    public int deleteSevaFunctionModuleByIds(Long[] ids)
    {
        return sevaFunctionModuleMapper.deleteSevaFunctionModuleByIds(ids);
    }

    /**
     * 删除功能模块信息
     *
     * @param id 功能模块主键
     * @return 结果
     */
    @Override
    public int deleteSevaFunctionModuleById(Long id)
    {
        return sevaFunctionModuleMapper.deleteSevaFunctionModuleById(id);
    }

    @Override
    public Double calculate(Long projectId) {
        SevaFunctionModule module = new SevaFunctionModule();
        module.setProjectId(projectId);
        List<SevaFunctionModule> poList =
                sevaFunctionModuleMapper.selectSevaFunctionModuleList(module);
        for(SevaFunctionModule po:poList){
            if(po.getIsHandle().equals("0")) return -1.0;
        }
        List<SevaFunctionModuleTypeResultDTO> list = sevaFunctionModuleMapper.calculate(projectId);
        Double calResult=0.0;
        for(SevaFunctionModuleTypeResultDTO dto:list){
            calResult+=dto.getFunctionTypeCost();
        }
        SevaParameter parameter =
                sevaParameterMapper.selectSevaParameterByName("costRatePersonMonth");
        Double costRatePersonMonth = Double.parseDouble(parameter.getParameterValue());

        return calResult*costRatePersonMonth;
    }
}
