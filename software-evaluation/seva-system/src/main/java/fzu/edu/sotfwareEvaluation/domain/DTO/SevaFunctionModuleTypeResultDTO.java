package fzu.edu.sotfwareEvaluation.domain.DTO;

import java.io.Serializable;

/**
 * @author JayerListen
 * @create 2024-08-14 2024/8/14
 */
public class SevaFunctionModuleTypeResultDTO implements Serializable {
    /** id */
    private Long id;

    /** 模块名称 */
    private String funtionName;

    /** 模块内容 */
    private String functionDetail;

    /** 项目id */
    private Long projectId;

    /** 模块分类 */
    private Long functionTypeId;

    /** 是否处理 */
    private String isHandle;

    /** 类型编码 */
    private String functionTypeCode;

    /** 类型名称 */
    private String functionTypeName;

    /** 类型描述 */
    private String functionTypeDesc;

    /** 类型关键字 */
    private String functionTypeKey;

    /** 基准工作量 */
    private Double functionTypeCost;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFuntionName() {
        return funtionName;
    }

    public void setFuntionName(String funtionName) {
        this.funtionName = funtionName;
    }

    public String getFunctionDetail() {
        return functionDetail;
    }

    public void setFunctionDetail(String functionDetail) {
        this.functionDetail = functionDetail;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getFunctionTypeId() {
        return functionTypeId;
    }

    public void setFunctionTypeId(Long functionTypeId) {
        this.functionTypeId = functionTypeId;
    }

    public String getIsHandle() {
        return isHandle;
    }

    public void setIsHandle(String isHandle) {
        this.isHandle = isHandle;
    }

    public String getFunctionTypeCode() {
        return functionTypeCode;
    }

    public void setFunctionTypeCode(String functionTypeCode) {
        this.functionTypeCode = functionTypeCode;
    }

    public String getFunctionTypeName() {
        return functionTypeName;
    }

    public void setFunctionTypeName(String functionTypeName) {
        this.functionTypeName = functionTypeName;
    }

    public String getFunctionTypeDesc() {
        return functionTypeDesc;
    }

    public void setFunctionTypeDesc(String functionTypeDesc) {
        this.functionTypeDesc = functionTypeDesc;
    }

    public String getFunctionTypeKey() {
        return functionTypeKey;
    }

    public void setFunctionTypeKey(String functionTypeKey) {
        this.functionTypeKey = functionTypeKey;
    }

    public Double getFunctionTypeCost() {
        return functionTypeCost;
    }

    public void setFunctionTypeCost(Double functionTypeCost) {
        this.functionTypeCost = functionTypeCost;
    }
}
