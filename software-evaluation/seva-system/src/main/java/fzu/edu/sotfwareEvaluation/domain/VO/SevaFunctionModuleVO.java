package fzu.edu.sotfwareEvaluation.domain.VO;

import fzu.edu.common.annotation.Excel;

import java.io.Serializable;

/**
 * @author JayerListen
 * @create 2024-08-13 2024/8/13
 */
public class SevaFunctionModuleVO  implements Serializable {
    /** id */
    private Long id;

    /** 模块名称 */
    @Excel(name = "模块名称")
    private String funtionName;

    /** 模块内容 */
    @Excel(name = "模块内容")
    private String functionDetail;

    /** 项目id */
    private Long projectId;

    /** 模块分类id */
    private Long functionTypeId;

    /** 是否处理 */
    private String isHandle;

    /** 模块分类名称 */
    @Excel(name = "模块分类")
    private String functionTypeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFuntionName() {
        return funtionName;
    }

    public void setFuntionName(String funtionName) {
        this.funtionName = funtionName;
    }

    public String getFunctionDetail() {
        return functionDetail;
    }

    public void setFunctionDetail(String functionDetail) {
        this.functionDetail = functionDetail;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getFunctionTypeId() {
        return functionTypeId;
    }

    public void setFunctionTypeId(Long functionTypeId) {
        this.functionTypeId = functionTypeId;
    }

    public String getIsHandle() {
        return isHandle;
    }

    public void setIsHandle(String isHandle) {
        this.isHandle = isHandle;
    }

    public String getFunctionTypeName() {
        return functionTypeName;
    }

    public void setFunctionTypeName(String functionTypeName) {
        this.functionTypeName = functionTypeName;
    }
}
