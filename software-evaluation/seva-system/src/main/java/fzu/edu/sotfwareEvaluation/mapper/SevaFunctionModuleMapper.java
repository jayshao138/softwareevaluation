package fzu.edu.sotfwareEvaluation.mapper;

import java.util.List;

import fzu.edu.sotfwareEvaluation.domain.DTO.SevaFunctionModuleTypeResultDTO;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionModule;

/**
 * 功能模块Mapper接口
 *
 * @author JayerListen
 * @date 2024-08-10
 */
public interface SevaFunctionModuleMapper
{
    /**
     * 查询功能模块
     *
     * @param id 功能模块主键
     * @return 功能模块
     */
    public SevaFunctionModule selectSevaFunctionModuleById(Long id);

    /**
     * 查询功能模块列表
     *
     * @param sevaFunctionModule 功能模块
     * @return 功能模块集合
     */
    public List<SevaFunctionModule> selectSevaFunctionModuleList(SevaFunctionModule sevaFunctionModule);

    /**
     * 新增功能模块
     *
     * @param sevaFunctionModule 功能模块
     * @return 结果
     */
    public int insertSevaFunctionModule(SevaFunctionModule sevaFunctionModule);

    /**
     * 修改功能模块
     *
     * @param sevaFunctionModule 功能模块
     * @return 结果
     */
    public int updateSevaFunctionModule(SevaFunctionModule sevaFunctionModule);

    /**
     * 删除功能模块
     *
     * @param id 功能模块主键
     * @return 结果
     */
    public int deleteSevaFunctionModuleById(Long id);

    /**
     * 批量删除功能模块
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSevaFunctionModuleByIds(Long[] ids);


    public List<SevaFunctionModuleTypeResultDTO> calculate(Long id);
}
