package fzu.edu.sotfwareEvaluation.domain;

import fzu.edu.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import fzu.edu.common.core.domain.BaseEntity;

/**
 * 行业基准参数对象 seva_parameter
 *
 * @author jayerlisten
 * @date 2024-04-01
 */
public class SevaParameter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 基准参数变量名 */
    @Excel(name = "基准参数变量名")
    private String parameterName;

    /** 基准参数名称 */
    @Excel(name = "基准参数名称")
    private String parameterLabel;

    /** 基准参数名称 */
    @Excel(name = "基准参数名称")
    private String parameterValue;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setParameterName(String parameterName)
    {
        this.parameterName = parameterName;
    }

    public String getParameterName()
    {
        return parameterName;
    }
    public void setParameterLabel(String parameterLabel)
    {
        this.parameterLabel = parameterLabel;
    }

    public String getParameterLabel()
    {
        return parameterLabel;
    }
    public void setParameterValue(String parameterValue)
    {
        this.parameterValue = parameterValue;
    }

    public String getParameterValue()
    {
        return parameterValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parameterName", getParameterName())
            .append("parameterLabel", getParameterLabel())
            .append("parameterValue", getParameterValue())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
