package fzu.edu.sotfwareEvaluation.service.impl;

import java.util.List;
import fzu.edu.common.utils.DateUtils;
import fzu.edu.sotfwareEvaluation.mapper.SevaFunctionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fzu.edu.sotfwareEvaluation.domain.SevaFunction;
import fzu.edu.sotfwareEvaluation.service.ISevaFunctionService;

/**
 * 功能模块Service业务层处理
 *
 * @author jayerListen
 * @date 2024-04-02
 */
@Service
public class SevaFunctionServiceImpl implements ISevaFunctionService
{
    @Autowired
    private SevaFunctionMapper sevaFunctionMapper;

    /**
     * 查询功能模块
     *
     * @param id 功能模块主键
     * @return 功能模块
     */
    @Override
    public SevaFunction selectSevaFunctionById(Long id)
    {
        return sevaFunctionMapper.selectSevaFunctionById(id);
    }

    /**
     * 查询功能模块列表
     *
     * @param sevaFunction 功能模块
     * @return 功能模块
     */
    @Override
    public List<SevaFunction> selectSevaFunctionList(SevaFunction sevaFunction)
    {
        return sevaFunctionMapper.selectSevaFunctionList(sevaFunction);
    }

    /**
     * 新增功能模块
     *
     * @param sevaFunction 功能模块
     * @return 结果
     */
    @Override
    public int insertSevaFunction(SevaFunction sevaFunction)
    {
        sevaFunction.setCreateTime(DateUtils.getNowDate());
        return sevaFunctionMapper.insertSevaFunction(sevaFunction);
    }

    /**
     * 修改功能模块
     *
     * @param sevaFunction 功能模块
     * @return 结果
     */
    @Override
    public int updateSevaFunction(SevaFunction sevaFunction)
    {
        sevaFunction.setUpdateTime(DateUtils.getNowDate());
        return sevaFunctionMapper.updateSevaFunction(sevaFunction);
    }

    /**
     * 批量删除功能模块
     *
     * @param ids 需要删除的功能模块主键
     * @return 结果
     */
    @Override
    public int deleteSevaFunctionByIds(Long[] ids)
    {
        return sevaFunctionMapper.deleteSevaFunctionByIds(ids);
    }

    /**
     * 删除功能模块信息
     *
     * @param id 功能模块主键
     * @return 结果
     */
    @Override
    public int deleteSevaFunctionById(Long id)
    {
        return sevaFunctionMapper.deleteSevaFunctionById(id);
    }
}
