package fzu.edu.sotfwareEvaluation.service;

import java.util.List;

import fzu.edu.sotfwareEvaluation.domain.DTO.SevaFunctionModuleDTO;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionModule;
import fzu.edu.sotfwareEvaluation.domain.VO.SevaFunctionModuleVO;

/**
 * 功能模块Service接口
 *
 * @author JayerListen
 * @date 2024-08-10
 */
public interface ISevaFunctionModuleService
{
    /**
     * 查询功能模块
     *
     * @param id 功能模块主键
     * @return 功能模块
     */
    public SevaFunctionModule selectSevaFunctionModuleById(Long id);

    /**
     * 查询功能模块列表
     *
     * @param sevaFunctionModule 功能模块
     * @return 功能模块集合
     */
    public List<SevaFunctionModuleVO> selectSevaFunctionModuleList(SevaFunctionModule sevaFunctionModule);

    /**
     * 新增功能模块
     *
     * @param sevaFunctionModule 功能模块
     * @return 结果
     */
    public int insertSevaFunctionModule(SevaFunctionModule sevaFunctionModule);

    /**
     * 新增功能模块
     *
     * @param list 功能模块列表
     * @return 结果
     */
    public int batchInsertSevaFunctionModule(List<SevaFunctionModuleDTO> list, Long projectId);

    /**
     * 修改功能模块
     *
     * @param sevaFunctionModule 功能模块
     * @return 结果
     */
    public int updateSevaFunctionModule(SevaFunctionModule sevaFunctionModule);

    /**
     * 批量删除功能模块
     *
     * @param ids 需要删除的功能模块主键集合
     * @return 结果
     */
    public int deleteSevaFunctionModuleByIds(Long[] ids);

    /**
     * 删除功能模块信息
     *
     * @param id 功能模块主键
     * @return 结果
     */
    public int deleteSevaFunctionModuleById(Long id);

    public Double calculate(Long projectId);
}
