package fzu.edu.sotfwareEvaluation.service.impl;

import java.util.List;
import fzu.edu.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fzu.edu.sotfwareEvaluation.mapper.SevaParameterMapper;
import fzu.edu.sotfwareEvaluation.domain.SevaParameter;
import fzu.edu.sotfwareEvaluation.service.ISevaParameterService;

/**
 * 行业基准参数Service业务层处理
 *
 * @author jayerlisten
 * @date 2024-04-01
 */
@Service
public class SevaParameterServiceImpl implements ISevaParameterService
{
    @Autowired
    private SevaParameterMapper sevaParameterMapper;

    /**
     * 查询行业基准参数
     *
     * @param id 行业基准参数主键
     * @return 行业基准参数
     */
    @Override
    public SevaParameter selectSevaParameterById(Long id)
    {
        return sevaParameterMapper.selectSevaParameterById(id);
    }

    /**
     * 查询行业基准参数列表
     *
     * @param sevaParameter 行业基准参数
     * @return 行业基准参数
     */
    @Override
    public List<SevaParameter> selectSevaParameterList(SevaParameter sevaParameter)
    {
        return sevaParameterMapper.selectSevaParameterList(sevaParameter);
    }

    /**
     * 新增行业基准参数
     *
     * @param sevaParameter 行业基准参数
     * @return 结果
     */
    @Override
    public int insertSevaParameter(SevaParameter sevaParameter)
    {
        sevaParameter.setCreateTime(DateUtils.getNowDate());
        return sevaParameterMapper.insertSevaParameter(sevaParameter);
    }

    /**
     * 修改行业基准参数
     *
     * @param sevaParameter 行业基准参数
     * @return 结果
     */
    @Override
    public int updateSevaParameter(SevaParameter sevaParameter)
    {
        sevaParameter.setUpdateTime(DateUtils.getNowDate());
        return sevaParameterMapper.updateSevaParameter(sevaParameter);
    }

    /**
     * 批量删除行业基准参数
     *
     * @param ids 需要删除的行业基准参数主键
     * @return 结果
     */
    @Override
    public int deleteSevaParameterByIds(Long[] ids)
    {
        return sevaParameterMapper.deleteSevaParameterByIds(ids);
    }

    /**
     * 删除行业基准参数信息
     *
     * @param id 行业基准参数主键
     * @return 结果
     */
    @Override
    public int deleteSevaParameterById(Long id)
    {
        return sevaParameterMapper.deleteSevaParameterById(id);
    }
}
