package fzu.edu.sotfwareEvaluation.service.impl;

import java.util.List;
import fzu.edu.common.utils.DateUtils;
import fzu.edu.sotfwareEvaluation.service.ISevaFunctionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fzu.edu.sotfwareEvaluation.mapper.SevaFunctionTypeMapper;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionType;

/**
 * 基线速查表Service业务层处理
 *
 * @author JayerListen
 * @date 2024-08-12
 */
@Service
public class SevaFunctionTypeServiceImpl implements ISevaFunctionTypeService
{
    @Autowired
    private SevaFunctionTypeMapper sevaFunctionTypeMapper;

    /**
     * 查询基线速查表
     *
     * @param id 基线速查表主键
     * @return 基线速查表
     */
    @Override
    public SevaFunctionType selectSevaFunctionTypeById(Long id)
    {
        return sevaFunctionTypeMapper.selectSevaFunctionTypeById(id);
    }

    /**
     * 查询基线速查表列表
     *
     * @param sevaFunctionType 基线速查表
     * @return 基线速查表
     */
    @Override
    public List<SevaFunctionType> selectSevaFunctionTypeList(SevaFunctionType sevaFunctionType)
    {
        return sevaFunctionTypeMapper.selectSevaFunctionTypeList(sevaFunctionType);
    }

    /**
     * 新增基线速查表
     *
     * @param sevaFunctionType 基线速查表
     * @return 结果
     */
    @Override
    public int insertSevaFunctionType(SevaFunctionType sevaFunctionType)
    {
        sevaFunctionType.setCreateTime(DateUtils.getNowDate());
        return sevaFunctionTypeMapper.insertSevaFunctionType(sevaFunctionType);
    }

    /**
     * 修改基线速查表
     *
     * @param sevaFunctionType 基线速查表
     * @return 结果
     */
    @Override
    public int updateSevaFunctionType(SevaFunctionType sevaFunctionType)
    {
        sevaFunctionType.setUpdateTime(DateUtils.getNowDate());
        return sevaFunctionTypeMapper.updateSevaFunctionType(sevaFunctionType);
    }

    /**
     * 批量删除基线速查表
     *
     * @param ids 需要删除的基线速查表主键
     * @return 结果
     */
    @Override
    public int deleteSevaFunctionTypeByIds(Long[] ids)
    {
        return sevaFunctionTypeMapper.deleteSevaFunctionTypeByIds(ids);
    }

    /**
     * 删除基线速查表信息
     *
     * @param id 基线速查表主键
     * @return 结果
     */
    @Override
    public int deleteSevaFunctionTypeById(Long id)
    {
        return sevaFunctionTypeMapper.deleteSevaFunctionTypeById(id);
    }
}
