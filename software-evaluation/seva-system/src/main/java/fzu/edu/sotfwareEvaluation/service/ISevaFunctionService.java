package fzu.edu.sotfwareEvaluation.service;

import java.util.List;
import fzu.edu.sotfwareEvaluation.domain.SevaFunction;

/**
 * 功能模块Service接口
 *
 * @author jayerListen
 * @date 2024-04-02
 */
public interface ISevaFunctionService
{
    /**
     * 查询功能模块
     *
     * @param id 功能模块主键
     * @return 功能模块
     */
    public SevaFunction selectSevaFunctionById(Long id);

    /**
     * 查询功能模块列表
     *
     * @param sevaFunction 功能模块
     * @return 功能模块集合
     */
    public List<SevaFunction> selectSevaFunctionList(SevaFunction sevaFunction);

    /**
     * 新增功能模块
     *
     * @param sevaFunction 功能模块
     * @return 结果
     */
    public int insertSevaFunction(SevaFunction sevaFunction);

    /**
     * 修改功能模块
     *
     * @param sevaFunction 功能模块
     * @return 结果
     */
    public int updateSevaFunction(SevaFunction sevaFunction);

    /**
     * 批量删除功能模块
     *
     * @param ids 需要删除的功能模块主键集合
     * @return 结果
     */
    public int deleteSevaFunctionByIds(Long[] ids);

    /**
     * 删除功能模块信息
     *
     * @param id 功能模块主键
     * @return 结果
     */
    public int deleteSevaFunctionById(Long id);
}
