package fzu.edu.sotfwareEvaluation.domain;

import fzu.edu.common.annotation.Excel;
import fzu.edu.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 基线速查表对象 seva_function_type
 *
 * @author JayerListen
 * @date 2024-08-12
 */
public class SevaFunctionType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 类型编码 */
    @Excel(name = "类型编码")
    private String functionTypeCode;

    /** 类型名称 */
    @Excel(name = "类型名称")
    private String functionTypeName;

    /** 类型描述 */
    @Excel(name = "类型描述")
    private String functionTypeDesc;

    /** 类型关键字 */
    @Excel(name = "类型关键字")
    private String functionTypeKey;

    /** 基准工作量 */
    @Excel(name = "基准工作量")
    private Double functionTypeCost;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setFunctionTypeCode(String functionTypeCode)
    {
        this.functionTypeCode = functionTypeCode;
    }

    public String getFunctionTypeCode()
    {
        return functionTypeCode;
    }
    public void setFunctionTypeName(String functionTypeName)
    {
        this.functionTypeName = functionTypeName;
    }

    public String getFunctionTypeName()
    {
        return functionTypeName;
    }
    public void setFunctionTypeDesc(String functionTypeDesc)
    {
        this.functionTypeDesc = functionTypeDesc;
    }

    public String getFunctionTypeDesc()
    {
        return functionTypeDesc;
    }
    public void setFunctionTypeKey(String functionTypeKey)
    {
        this.functionTypeKey = functionTypeKey;
    }

    public String getFunctionTypeKey()
    {
        return functionTypeKey;
    }

    public Double getFunctionTypeCost() {
        return functionTypeCost;
    }

    public void setFunctionTypeCost(Double functionTypeCost) {
        this.functionTypeCost = functionTypeCost;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("functionTypeCode", getFunctionTypeCode())
            .append("functionTypeName", getFunctionTypeName())
            .append("functionTypeDesc", getFunctionTypeDesc())
            .append("functionTypeKey", getFunctionTypeKey())
            .append("functionTypeCost", getFunctionTypeCost())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
