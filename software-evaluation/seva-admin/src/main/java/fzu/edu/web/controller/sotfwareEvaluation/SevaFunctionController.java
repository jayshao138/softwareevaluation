package fzu.edu.web.controller.sotfwareEvaluation;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import fzu.edu.common.core.controller.BaseController;
import fzu.edu.common.core.domain.AjaxResult;
import fzu.edu.common.enums.BusinessType;
import fzu.edu.common.utils.poi.ExcelUtil;
import fzu.edu.sotfwareEvaluation.domain.SevaFunction;
import fzu.edu.sotfwareEvaluation.service.ISevaFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fzu.edu.common.annotation.Log;

/**
 * 功能模块Controller
 *
 * @author jayerListen
 * @date 2024-04-02
 */
@RestController
@RequestMapping("/sotfwareEvaluation/function")
public class SevaFunctionController extends BaseController
{
    @Autowired
    private ISevaFunctionService sevaFunctionService;

    /**
     * 查询功能模块列表
     */
    @GetMapping("/list")
    public AjaxResult list(SevaFunction sevaFunction)
    {
        List<SevaFunction> list = sevaFunctionService.selectSevaFunctionList(sevaFunction);
        return success(list);
    }

    /**
     * 导出功能模块列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, SevaFunction sevaFunction)
    {
        List<SevaFunction> list = sevaFunctionService.selectSevaFunctionList(sevaFunction);
        ExcelUtil<SevaFunction> util = new ExcelUtil<SevaFunction>(SevaFunction.class);
        util.exportExcel(response, list, "功能模块数据");
    }

    /**
     * 获取功能模块详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sevaFunctionService.selectSevaFunctionById(id));
    }

    /**
     * 新增功能模块
     */
    @Log(title = "功能模块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SevaFunction sevaFunction)
    {
        return toAjax(sevaFunctionService.insertSevaFunction(sevaFunction));
    }

    /**
     * 修改功能模块
     */
    @Log(title = "功能模块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SevaFunction sevaFunction)
    {
        return toAjax(sevaFunctionService.updateSevaFunction(sevaFunction));
    }

    /**
     * 删除功能模块
     */
    @Log(title = "功能模块", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sevaFunctionService.deleteSevaFunctionByIds(ids));
    }
}
