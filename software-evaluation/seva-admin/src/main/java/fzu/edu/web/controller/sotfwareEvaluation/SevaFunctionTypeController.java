package fzu.edu.web.controller.sotfwareEvaluation;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import fzu.edu.common.core.controller.BaseController;
import fzu.edu.common.core.domain.AjaxResult;
import fzu.edu.common.core.page.TableDataInfo;
import fzu.edu.sotfwareEvaluation.service.ISevaFunctionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionType;
import fzu.edu.common.utils.poi.ExcelUtil;

/**
 * 基线速查表Controller
 *
 * @author JayerListen
 * @date 2024-08-12
 */
@RestController
@RequestMapping("/sotfwareEvaluation/functionType")
public class SevaFunctionTypeController extends BaseController
{
    @Autowired
    private ISevaFunctionTypeService sevaFunctionTypeService;

    /**
     * 查询基线速查表列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SevaFunctionType sevaFunctionType)
    {
        startPage();
        List<SevaFunctionType> list = sevaFunctionTypeService.selectSevaFunctionTypeList(sevaFunctionType);
        return getDataTable(list);
    }

    /**
     * 导出基线速查表列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, SevaFunctionType sevaFunctionType)
    {
        List<SevaFunctionType> list = sevaFunctionTypeService.selectSevaFunctionTypeList(sevaFunctionType);
        ExcelUtil<SevaFunctionType> util = new ExcelUtil<SevaFunctionType>(SevaFunctionType.class);
        util.exportExcel(response, list, "基线速查表数据");
    }

    /**
     * 获取基线速查表详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sevaFunctionTypeService.selectSevaFunctionTypeById(id));
    }

    /**
     * 新增基线速查表
     */
    @PostMapping
    public AjaxResult add(@RequestBody SevaFunctionType sevaFunctionType)
    {
        return toAjax(sevaFunctionTypeService.insertSevaFunctionType(sevaFunctionType));
    }

    /**
     * 修改基线速查表
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SevaFunctionType sevaFunctionType)
    {
        return toAjax(sevaFunctionTypeService.updateSevaFunctionType(sevaFunctionType));
    }

    /**
     * 删除基线速查表
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sevaFunctionTypeService.deleteSevaFunctionTypeByIds(ids));
    }
}
