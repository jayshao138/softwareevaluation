package fzu.edu.web.controller.sotfwareEvaluation;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import fzu.edu.common.core.controller.BaseController;
import fzu.edu.common.core.domain.AjaxResult;
import fzu.edu.common.core.page.TableDataInfo;
import fzu.edu.sotfwareEvaluation.domain.SevaParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fzu.edu.sotfwareEvaluation.service.ISevaParameterService;
import fzu.edu.common.utils.poi.ExcelUtil;

/**
 * 行业基准参数Controller
 *
 * @author jayerlisten
 * @date 2024-04-01
 */
@RestController
@RequestMapping("/sotfwareEvaluation/parameter")
public class SevaParameterController extends BaseController
{
    @Autowired
    private ISevaParameterService sevaParameterService;

    /**
     * 查询行业基准参数列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SevaParameter sevaParameter)
    {
        startPage();
        List<SevaParameter> list = sevaParameterService.selectSevaParameterList(sevaParameter);
        return getDataTable(list);
    }

    /**
     * 导出行业基准参数列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, SevaParameter sevaParameter)
    {
        List<SevaParameter> list = sevaParameterService.selectSevaParameterList(sevaParameter);
        ExcelUtil<SevaParameter> util = new ExcelUtil<SevaParameter>(SevaParameter.class);
        util.exportExcel(response, list, "行业基准参数数据");
    }

    /**
     * 获取行业基准参数详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sevaParameterService.selectSevaParameterById(id));
    }

    /**
     * 新增行业基准参数
     */
    @PostMapping
    public AjaxResult add(@RequestBody SevaParameter sevaParameter)
    {
        return toAjax(sevaParameterService.insertSevaParameter(sevaParameter));
    }

    /**
     * 修改行业基准参数
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SevaParameter sevaParameter)
    {
        return toAjax(sevaParameterService.updateSevaParameter(sevaParameter));
    }

    /**
     * 删除行业基准参数
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sevaParameterService.deleteSevaParameterByIds(ids));
    }
}
