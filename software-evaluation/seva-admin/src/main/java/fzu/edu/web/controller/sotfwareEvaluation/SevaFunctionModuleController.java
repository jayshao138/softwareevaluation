package fzu.edu.web.controller.sotfwareEvaluation;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import fzu.edu.sotfwareEvaluation.domain.DTO.SevaFunctionModuleDTO;
import fzu.edu.sotfwareEvaluation.domain.VO.SevaFunctionModuleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fzu.edu.common.core.controller.BaseController;
import fzu.edu.common.core.domain.AjaxResult;
import fzu.edu.sotfwareEvaluation.domain.SevaFunctionModule;
import fzu.edu.sotfwareEvaluation.service.ISevaFunctionModuleService;
import fzu.edu.common.utils.poi.ExcelUtil;
import fzu.edu.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 功能模块Controller
 *
 * @author JayerListen
 * @date 2024-08-10
 */
@RestController
@RequestMapping("/sotfwareEvaluation/functionModule")
public class SevaFunctionModuleController extends BaseController
{
    @Autowired
    private ISevaFunctionModuleService sevaFunctionModuleService;

    /**
     * 查询功能模块列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SevaFunctionModule sevaFunctionModule)
    {
        startPage();
        List<SevaFunctionModuleVO> list = sevaFunctionModuleService.selectSevaFunctionModuleList(sevaFunctionModule);
        return getDataTable(list);
    }

    /**
     * 导出功能模块列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, SevaFunctionModule sevaFunctionModule)
    {
        List<SevaFunctionModuleVO> list = sevaFunctionModuleService.selectSevaFunctionModuleList(sevaFunctionModule);
        ExcelUtil<SevaFunctionModuleVO> util = new ExcelUtil<SevaFunctionModuleVO>(SevaFunctionModuleVO.class);
        util.exportExcel(response, list, "功能模块数据");
    }

    /**
     * 获取功能模块详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sevaFunctionModuleService.selectSevaFunctionModuleById(id));
    }

    /**
     * 新增功能模块
     */
    @PostMapping
    public AjaxResult add(@RequestBody SevaFunctionModule sevaFunctionModule)
    {
        return toAjax(sevaFunctionModuleService.insertSevaFunctionModule(sevaFunctionModule));
    }

    /**
     * 修改功能模块
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SevaFunctionModule sevaFunctionModule)
    {
        return toAjax(sevaFunctionModuleService.updateSevaFunctionModule(sevaFunctionModule));
    }

    /**
     * 删除功能模块
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sevaFunctionModuleService.deleteSevaFunctionModuleByIds(ids));
    }

    /**
     * 下载模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<SevaFunctionModuleDTO> util = new ExcelUtil<SevaFunctionModuleDTO>(SevaFunctionModuleDTO.class);
        util.importTemplateExcel(response, "功能模块数据");
    }

    /**
     * 导入数据
     */
    @PostMapping("/importData/{projectId}")
    public AjaxResult importData(MultipartFile file, boolean updateSupport,
                                 @PathVariable("projectId") Long projectId) throws Exception
    {
        ExcelUtil<SevaFunctionModuleDTO> util = new ExcelUtil<SevaFunctionModuleDTO>(SevaFunctionModuleDTO.class);
        List<SevaFunctionModuleDTO> list = util.importExcel(file.getInputStream());
        int i = sevaFunctionModuleService.batchInsertSevaFunctionModule(list,projectId);

        if(i>0) return success("导入成功");
        else return error("导入失败");
    }

    /**
     * 汇总计算
     */
    @PostMapping("/calculate/{projectId}")
    public AjaxResult calculate(@PathVariable("projectId") Long projectId) throws Exception
    {
        Double result = sevaFunctionModuleService.calculate(projectId);
        if(result<0) return error("计算失败, 还有模块未分类.");
        else return success().put("result",result);
    }
}
