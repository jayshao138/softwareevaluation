package fzu.edu.web.controller.sotfwareEvaluation;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import fzu.edu.common.core.controller.BaseController;
import fzu.edu.common.core.domain.AjaxResult;
import fzu.edu.common.core.page.TableDataInfo;
import fzu.edu.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fzu.edu.sotfwareEvaluation.domain.SevaProject;
import fzu.edu.sotfwareEvaluation.service.ISevaProjectService;

/**
 * 项目管理Controller
 *
 * @author jayerListen
 * @date 2024-04-01
 */
@RestController
@RequestMapping("/sotfwareEvaluation/project")
public class SevaProjectController extends BaseController
{
    @Autowired
    private ISevaProjectService sevaProjectService;

    /**
     * 查询项目管理列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SevaProject sevaProject)
    {
        startPage();
        List<SevaProject> list = sevaProjectService.selectSevaProjectList(sevaProject);
        return getDataTable(list);
    }

    /**
     * 导出项目管理列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, SevaProject sevaProject)
    {
        List<SevaProject> list = sevaProjectService.selectSevaProjectList(sevaProject);
        ExcelUtil<SevaProject> util = new ExcelUtil<SevaProject>(SevaProject.class);
        util.exportExcel(response, list, "项目管理数据");
    }

    /**
     * 获取项目管理详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sevaProjectService.selectSevaProjectById(id));
    }

    /**
     * 新增项目管理
     */
    @PostMapping
    public AjaxResult add(@RequestBody SevaProject sevaProject)
    {
        return toAjax(sevaProjectService.insertSevaProject(sevaProject));
    }

    /**
     * 修改项目管理
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SevaProject sevaProject)
    {
        return toAjax(sevaProjectService.updateSevaProject(sevaProject));
    }

    /**
     * 删除项目管理
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sevaProjectService.deleteSevaProjectByIds(ids));
    }
}
