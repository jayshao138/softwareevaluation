import request from '@/utils/request'

// 查询基线速查表列表
export function listFunctionType(query) {
  return request({
    url: '/sotfwareEvaluation/functionType/list',
    method: 'get',
    params: query
  })
}

// 查询基线速查表详细
export function getFunctionType(id) {
  return request({
    url: '/sotfwareEvaluation/functionType/' + id,
    method: 'get'
  })
}

// 新增基线速查表
export function addFunctionType(data) {
  return request({
    url: '/sotfwareEvaluation/functionType',
    method: 'post',
    data: data
  })
}

// 修改基线速查表
export function updateFunctionType(data) {
  return request({
    url: '/sotfwareEvaluation/functionType',
    method: 'put',
    data: data
  })
}

// 删除基线速查表
export function delFunctionType(id) {
  return request({
    url: '/sotfwareEvaluation/functionType/' + id,
    method: 'delete'
  })
}
