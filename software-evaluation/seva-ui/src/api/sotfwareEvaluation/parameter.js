import request from '@/utils/request'

// 查询行业基准参数列表
export function listParameter(query) {
  return request({
    url: '/sotfwareEvaluation/parameter/list',
    method: 'get',
    params: query
  })
}

// 查询行业基准参数详细
export function getParameter(id) {
  return request({
    url: '/sotfwareEvaluation/parameter/' + id,
    method: 'get'
  })
}

// 新增行业基准参数
export function addParameter(data) {
  return request({
    url: '/sotfwareEvaluation/parameter',
    method: 'post',
    data: data
  })
}

// 修改行业基准参数
export function updateParameter(data) {
  return request({
    url: '/sotfwareEvaluation/parameter',
    method: 'put',
    data: data
  })
}

// 删除行业基准参数
export function delParameter(id) {
  return request({
    url: '/sotfwareEvaluation/parameter/' + id,
    method: 'delete'
  })
}
