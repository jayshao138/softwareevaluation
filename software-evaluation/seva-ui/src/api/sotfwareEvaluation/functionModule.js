import request from '@/utils/request'

// 查询功能模块列表
export function listFunctionModule(query) {
  return request({
    url: '/sotfwareEvaluation/functionModule/list',
    method: 'get',
    params: query
  })
}

// 查询功能模块详细
export function getFunctionModule(id) {
  return request({
    url: '/sotfwareEvaluation/functionModule/' + id,
    method: 'get'
  })
}

// 新增功能模块
export function addFunctionModule(data) {
  return request({
    url: '/sotfwareEvaluation/functionModule',
    method: 'post',
    data: data
  })
}

// 修改功能模块
export function updateFunctionModule(data) {
  return request({
    url: '/sotfwareEvaluation/functionModule',
    method: 'put',
    data: data
  })
}

// 删除功能模块
export function delFunctionModule(id) {
  return request({
    url: '/sotfwareEvaluation/functionModule/' + id,
    method: 'delete'
  })
}

// 汇总计算
export function calculate(projectId) {
  return request({
    url: '/sotfwareEvaluation/functionModule/calculate/' + projectId,
    method: 'post',
  })
}
