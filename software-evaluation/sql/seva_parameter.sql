﻿/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-docker-mysql
 Source Server Type    : MySQL
 Source Server Version : 50744
 Source Host           : 47.236.120.119:3306
 Source Schema         : fzu_software_evaluation

 Target Server Type    : MySQL
 Target Server Version : 50744
 File Encoding         : 65001

 Date: 01/04/2024 21:44:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for seva_parameter
-- ----------------------------
create table seva_function (
  id                bigint(20)      not null auto_increment    comment 'id',
  parent_id         bigint(20)      default 0                  comment '父模块id',
  ancestors         varchar(50)     default ''                 comment '祖级列表',
  funtion_name      varchar(30)     default ''                 comment '功能模块名称',
	funtion_detail    varchar(500)    default ''                 comment '功能描述',
	ilf    						int    					default 0                  comment '内部逻辑文件',
	eif    						int    					default 0                  comment '外部接口文件',
	ei    						int    					default 0                  comment '外部输入',
	eo    						int    					default 0                  comment '外部输出',
	eq    						int    					default 0                  comment '外部查询',
	type_factor				double					default 1									 comment '应用类型调节因子',
	repeat_factor			double					default 1									 comment '重复性调节因子',
	requirement_factor double					default 1									 comment '应用要求调节因子(又分布式/性能/可靠性/多重站点适配等因子计算决定)',
	distribution_factor double				default 0									 comment '分布式要求因子',
	performance_factor double					default 0									 comment '性能要求因子',
	reliability_factor double					default 0									 comment '可靠性要求因子',
	adaptive_factor 	double					default 0									 comment '多重站点适配要求因子',
	funtion_point			int																				 comment '功能点数',
  order_num         int(4)          default 0                  comment '显示顺序',
  is_leaf           char(1)         default '0'                comment '是否为叶节点（0正常 1停用）',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time 	    datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  primary key (id)
) engine=innodb auto_increment=200 comment = '功能模块表';
